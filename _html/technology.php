<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <title>Genelabsmedical</title>
    <?php include 'includes/common-doc-head.php'; ?>
</head>
<body>
    <?php include 'includes/page-header.php'; ?>

    <div class="container-fluid p-0">
        <div class="inner-page-banner" style="background-image: url('assets/images/technology-banner.jpg')">
            <div class="inner-page-banner-bg relative d-flex align-items-center">    
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="inner-banner-content">
                                <div class="text-center">
                                    <h1 class="text-center m-0">Technology</h1>
                                    <nav aria-label="breadcrumb" class="breadcrumb-nav">
                                        <ol class="breadcrumb m-0">
                                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                                            <li class="breadcrumb-item"><a href="#">About Us</a></li>
                                            <li class="breadcrumb-item active" aria-current="page">Technology</li>
                                        </ol>
                                    </nav>
                                </div> 
                            </div>                              
                        </div>                                               
                    </div>
                </div>  
                <span class="move-down"><i class="fa fa-angle-down" aria-hidden="true"></i></span>             
            </div>
        </div>
    </div>

    <main id="elements-page" class="main-content">

        <div class="page-section pt-0 pb-4 inner-page-content">
            <div class="technology">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12">
                            <p class="theme-description">DNA contains the blueprint of life, and codes for the molecular machineries that affect all the biological systems that create and maintains life. Reading the sequence of DNA is useful in understanding the structure and function of the cellular machineries, and elucidating the causes of diseases and response to treatments. We use Next Generation Sequencing (NGS) to read the DNA sequence, a platform that enables sequencing of thousands of millions of DNA molecules simultaneously.</p>

                            <p class="theme-description">Genelabs utilizes the NGS workflow of the Genestudio S5 NGS system launched by Thermo Fisher Scientific (USA) in 2018, offers speed, accuracy, and flexibility for different gene panels, tailored to research or disease areas of interest.  The Genestudio S5 uses the Ion Torrent™ technology that has been referenced in over 4,000 publications to date.</p>
                        </div>
                    </div>  
                </div>
            </div>
        </div>

    </main>

    <?php include 'includes/page-footer.php'; ?>
    <?php include 'includes/common-scripts.php'; ?>
</body>
</html>
