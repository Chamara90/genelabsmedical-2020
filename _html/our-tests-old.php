<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <title>Genelabsmedical</title>
    <?php include 'includes/common-doc-head.php'; ?>
</head>
<body>
    <?php include 'includes/page-header.php'; ?>

    <div class="container-fluid p-0">
        <div class="inner-page-banner" style="background-image: url('assets/images/out-tests-banner.jpg')">
            <div class="inner-page-banner-bg relative d-flex align-items-center">    
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="inner-banner-content">
                                <div class="text-center">
                                    <h1 class="text-center m-0">Our Tests</h1>
                                    <nav aria-label="breadcrumb" class="breadcrumb-nav">
                                        <ol class="breadcrumb m-0">
                                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                                            <li class="breadcrumb-item active" aria-current="page">Our Tests</li>
                                        </ol>
                                    </nav>
                                </div> 
                            </div>                              
                        </div>                                               
                    </div>
                </div>  
                <span class="move-down"><i class="fa fa-angle-down" aria-hidden="true"></i></span>             
            </div>
        </div>
    </div>

    <main id="elements-page" class="main-content">

        <div class="page-section pt-3 pb-4 inner-page-content">
            <div class="gene-tests">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12">
                            
                            <div class="gene-tests">

                                <ul class="tabs">
                                    <li class="active" rel="tab1">Non-invasive Prenatal Testing (NIPT)</li>
                                    <li rel="tab2">Pre-implantation Genetic Screening (PGS)</li>
                                    <li rel="tab3">Pre-implantation Genetic Diagnosis (PGD)</li>
                                    <li rel="tab4">BRCA 1 & 2 Panel</li>
                                    <li rel="tab5">Cancer Hospital Panel</li>
                                    <li rel="tab6">Lung Cancer Mutation Panel</li>
                                    <li rel="tab7">RAS Extended Plus for Colorectal Cancer</li>
                                    <li rel="tab8">Thyroid Cancer Mutation Profile</li>
                                    <li rel="tab9">AML Mutation Panel</li>
                                    <li rel="tab10">GIST Mutation Panel</li>

                                </ul>

                                <div class="tab_container">

                                    <h4 class="d_active tab_drawer_heading" rel="tab1">Non-invasive Prenatal Testing (NIPT)</h4>
                                    <div id="tab1" class="tab_content">                                        
                                        <h3>Non-invasive Prenatal Testing (NIPT)</h3>
                                        <p class="theme-description">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut 
                                        labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco 
                                        laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in 
                                        voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat 
                                        non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>            
                                        <h4 class="mt-4">How to get a Paternity Test</h4>
                                        <div>
                                            <ul class="test-step pl-3">
                                                <li>
                                                    <p class="step-tag">Step 01</p>
                                                    <p class="theme-description">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut 
                                                    labore et dolore magna aliqua.</p>
                                                </li>
                                                <li>
                                                    <p class="step-tag">Step 02</p>
                                                    <p class="theme-description">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut 
                                                    labore et dolore magna aliqua.</p>
                                                </li>
                                                <li>
                                                    <p class="step-tag">Step 03</p>
                                                    <p class="theme-description">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut 
                                                    labore et dolore magna aliqua.</p>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <!-- #tab1 -->

                                    <h4 class="tab_drawer_heading" rel="tab2">Pre-implantation Genetic Screening (PGS)</h4>
                                    <div id="tab2" class="tab_content">
                                        <h3>Pre-implantation Genetic Screening (PGS)</h3>
                                        <p class="theme-description">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut 
                                        labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco 
                                        laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in 
                                        voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat 
                                        non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>            
                                    </div>
                                    <!-- #tab2 -->

                                    <h4 class="tab_drawer_heading" rel="tab3">Pre-implantation Genetic Diagnosis (PGD)</h4>
                                    <div id="tab3" class="tab_content">
                                        <h3>Pre-implantation Genetic Diagnosis (PGD)</h3>
                                        <p class="theme-description">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut 
                                        labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco 
                                        laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in 
                                        voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat 
                                        non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>            
                                    </div>
                                    <!-- #tab3 -->

                                    <h4 class="tab_drawer_heading" rel="tab4">BRCA 1 & 2 Panel</h4>
                                    <div id="tab4" class="tab_content">
                                        <h3>BRCA 1 & 2 Panel</h3>
                                        <p class="theme-description">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut 
                                        labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco 
                                        laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in 
                                        voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat 
                                        non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>            
                                    </div>
                                    <!-- #tab4 --> 

                                    <h4 class="tab_drawer_heading" rel="tab5">Cancer Hospital Panel</h4>
                                    <div id="tab5" class="tab_content">
                                        <h3>Cancer Hospital Panel</h3>
                                        <p class="theme-description">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut 
                                        labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco 
                                        laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in 
                                        voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat 
                                        non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>            
                                    </div>
                                    <!-- #tab5 --> 

                                    <h4 class="tab_drawer_heading" rel="tab6">Lung Cancer Mutation Panel</h4>
                                    <div id="tab6" class="tab_content">
                                        <h3>Lung Cancer Mutation Panel</h3>
                                        <p class="theme-description">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut 
                                        labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco 
                                        laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in 
                                        voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat 
                                        non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>            
                                    </div>
                                    <!-- #tab6 --> 

                                    <h4 class="tab_drawer_heading" rel="tab7">RAS Extended Plus for Colorectal Cancer</h4>
                                    <div id="tab7" class="tab_content">
                                        <h3>RAS Extended Plus for Colorectal Cancer</h3>
                                        <p class="theme-description">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut 
                                        labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco 
                                        laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in 
                                        voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat 
                                        non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>            
                                        
                                    </div>
                                    <!-- #tab7 --> 

                                    <h4 class="tab_drawer_heading" rel="tab8">Thyroid Cancer Mutation Profile</h4>
                                    <div id="tab8" class="tab_content">
                                        <h3>Thyroid Cancer Mutation Profile</h3>
                                        <p class="theme-description">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut 
                                        labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco 
                                        laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in 
                                        voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat 
                                        non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>            
                                    </div>
                                    <!-- #tab8 --> 

                                    <h4 class="tab_drawer_heading" rel="tab9">AML Mutation Panel</h4>
                                    <div id="tab9" class="tab_content">
                                        <h3>AML Mutation Panel</h3>
                                        <p class="theme-description">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut 
                                        labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco 
                                        laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in 
                                        voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat 
                                        non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>            
                                    </div>
                                    <!-- #tab9 --> 

                                    <h4 class="tab_drawer_heading" rel="tab10">GIST Mutation Panel</h4>
                                    <div id="tab10" class="tab_content">
                                        <h3>GIST Mutation Panel</h3>
                                        <p class="theme-description">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut 
                                        labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco 
                                        laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in 
                                        voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat 
                                        non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>            
                                    </div>
                                    <!-- #tab10 --> 
                                    </div>
                                <!-- .tab_container -->
                                </div>
                                

                            </div>
                            
                        </div>
                    </div>  
                </div>
            </div>
        </div>

    </main>

    <?php include 'includes/page-footer.php'; ?>
    <?php include 'includes/common-scripts.php'; ?>

    <script>
        // tabbed content
    // http://www.entheosweb.com/tutorials/css/tabs.asp
    $(".tab_content").hide();
    $(".tab_content:first").show();

  /* if in tab mode */
    $("ul.tabs li").click(function() {
		
      $(".tab_content").hide();
      var activeTab = $(this).attr("rel"); 
      $("#"+activeTab).fadeIn();		
		
      $("ul.tabs li").removeClass("active");
      $(this).addClass("active");

	  $(".tab_drawer_heading").removeClass("d_active");
	  $(".tab_drawer_heading[rel^='"+activeTab+"']").addClass("d_active");
	  
    });
	/* if in drawer mode */
	$(".tab_drawer_heading").click(function() {
      
      $(".tab_content").hide();
      var d_activeTab = $(this).attr("rel"); 
      $("#"+d_activeTab).fadeIn();
	  
	  $(".tab_drawer_heading").removeClass("d_active");
      $(this).addClass("d_active");
	  
	  $("ul.tabs li").removeClass("active");
	  $("ul.tabs li[rel^='"+d_activeTab+"']").addClass("active");
    });
	
	
	/* Extra class "tab_last" 
	   to add border to right side
	   of last tab */
	$('ul.tabs li').last().addClass("tab_last");
        </script>
</body>
</html>
