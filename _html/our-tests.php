<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <title>Genelabsmedical</title>
    <?php include 'includes/common-doc-head.php'; ?>
</head>
<body>
    <?php include 'includes/page-header.php'; ?>

    <div class="container-fluid p-0">
        <div class="inner-page-banner" style="background-image: url('assets/images/out-tests-banner.jpg')">
            <div class="inner-page-banner-bg relative d-flex align-items-center">    
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="inner-banner-content">
                                <div class="text-center">
                                    <h1 class="text-center m-0">Our Tests</h1>
                                    <nav aria-label="breadcrumb" class="breadcrumb-nav">
                                        <ol class="breadcrumb m-0">
                                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                                            <li class="breadcrumb-item active" aria-current="page">Our Tests</li>
                                        </ol>
                                    </nav>
                                </div> 
                            </div>                              
                        </div>                                               
                    </div>
                </div>  
                <span class="move-down"><i class="fa fa-angle-down" aria-hidden="true"></i></span>             
            </div>
        </div>
    </div>

    <main id="elements-page" class="main-content">

        <div class="page-section pt-1 pb-4 inner-page-content">
            <div class="gene-tests">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12">
                            
                            <div class="gene-tests">

                                <ul class="tabs">
                                    <li class="tab-name active" rel="tab1">Non-invasive prenatal testing (NIPT)</li>
                                    <li class="tab-name" rel="tab2">Pre-implantation Genetic Screening (PGS)</li>
                                    <li class="tab-name"  rel="tab3">Pre-implantation Genetic Diagnosis (PGD)</li>
                                    <li class="tab-name"  rel="tab4">BRCA 1 & 2 panel</li>
                                    <li class="tab-name"  rel="tab5">Cancer hotspot panel</li>
                                    <li class="tab-name" rel="tab6">Lung Cancer mutation panel</li>
                                    <li class="tab-name" rel="tab7">RAS Extended Plus for Colorectal Cancer</li>
                                    <li class="tab-name tab-name-bt" rel="tab8">Thyroid cancer mutation profile</li>
                                    <li class="tab-name tab-name-bt" rel="tab9">AML mutation panel</li>
                                    <li class="tab-name tab-name-bt" rel="tab10">GIST Mutation Panel</li>

                                    <li class="tab-name tab-name-bt" rel="tab11">Prostate cancer mutation panel</li>
                                    <li class="tab-name tab-name-bt" rel="tab12">ABL1 TKI resistance mutations</li>
                                    <li class="tab-name tab-name-bt" rel="tab13">MPN mutations</li>
                                    <li class="tab-name tab-name-bt" rel="tab14">TP53 mutation</li>
                                    <li class="tab-name tab-name-bt" rel="tab16">IDH1 & IDH2 mutations for glioma</li>
                                    <li class="tab-name tab-name-bt" rel="tab17">KIT D816V mutation</li>
                                    <li class="tab-name tab-name-bt" rel="tab18">FGFR3 mutation for bladder cancer</li>
                                    <li class="tab-name tab-name-bt" rel="tab19">BCR-ABL detection</li>
                                    <li class="tab-name tab-name-bt" rel="tab20">PML-RARA detection</li>
                                    <li class="tab-name tab-name-bt" rel="tab21">ALL translocation panel</li>
                                    <li class="tab-name tab-name-bt" rel="tab22">AML translocation panel</li>
                                    <li class="tab-name tab-name-bt" rel="tab23">HLA typing</li>
                                    <li class="tab-name tab-name-bt" rel="tab24">Paternity testing</li>
                                </ul>

                                <div class="tab_container tab-scroll-to">

                                    <h4 class="d_active tab_drawer_heading" rel="tab1">Non-invasive prenatal testing (NIPT)</h4>
                                    <div id="tab1" class="tab_content">
                                        <h3>Non-invasive prenatal testing (NIPT)</h3>
                                        <p class="theme-description-nlsp">Check the DNA of your baby just after 10 weeks of pregnancy using NIPT (Non-invasive prenatal testing).  With a blood sample from the mother we can detect baby’s conditions like Down Syndrome, Edward Syndrome, Patau Syndrome, Turner Syndrome & Klinefelter syndrome with upto 99% accuracy. This test is specially recommended worldwide for mothers above 35 years, since the risk of getting these syndromes is increased with increasing maternal age.However it can be performed for anyone<br>Lanka to perform this complex test </p>                                                                                        
                                    </div>
                                    <!-- #tab1 -->

                                    <h4 class="tab_drawer_heading" rel="tab2">Pre-implantation genetic screening (PGS)</h4>
                                    <div id="tab2" class="tab_content">
                                        <h3>Pre-implantation genetic screening (PGS)</h3>
                                        <p class="theme-description-nlsp">Pre-implantation genetic screening (PGS, also called PGT-A) is a test designed for those undergoing IVF (in-vitro fertlization) procedures. PGS can identify the embryos that if implanted can lead to babies with syndromes like Down, Edward, Patau, Turner & Klinefelter. including IVF, surrogate, egg-donor and twin pregnancies.</p>            
                                        <p>Genelabs was the first laboratory in Sri Other benefits of this test are,</p>
                                        <ul class="test-list">
                                            <li><p>Higher chances of pregnancy</p></li>
                                            <li><p>Reduced risk of miscarriages</p></li>
                                            <li><p>More confidence in transferring a single embryo, avoiding health risks associated with twin or triplet pregnancies</p></li>
                                            <li><p>Reduced number of IVF cycles needed to achieve pregnancy, potentially reducing the time to pregnancy and the costs of extra cycles</p></li>
                                        </ul>
                                    </div>
                                    <!-- #tab2 -->

                                    <h4 class="tab_drawer_heading" rel="tab3">Pre-implantation genetic diagnosis (PGD)</h4>
                                    <div id="tab3" class="tab_content">
                                        <h3>Pre-implantation genetic diagnosis (PGD)</h3>
                                        <p class="theme-description-nlsp">Pre-implantation genetic diagnosis (PGD, also called PGT-M) is designed to identify embryos with inherited genetic abnormalities. These abnormalities (eg: thalassemia), present in one or both parents, could be passed to embryos.  During PGD, a small number of cells are removed from the embryo, and the DNA is tested to identify the embryos that have not got the disease gene from the parents.</p>            
                                        <p class="theme-description-nlsp">Couples who choose PGD could usually conceive naturally, but are using IVF specifically so that they can have healthy babies, and prevent passing on of the abnormal genes to subsequent generations.</p>            
                                    </div>
                                    <!-- #tab3 -->

                                    <h4 class="tab_drawer_heading" rel="tab4">BRCA 1 & 2 panel</h4>
                                    <div id="tab4" class="tab_content">
                                        <h3>BRCA 1 & 2 panel</h3>
                                        <p class="theme-description-nlsp">BRCA1 and BRCA2 are two related genes, the changes (mutations) of which have been known to increase the risk of breast, ovarian & prostate cancers. In general, about 69-72% of women who inherit a harmful BRCA1 or BRCA2 mutation will develop breast cancer, and 17-44% of women who inherit these mutations will develop ovarian cancer by the age of 80. BRCA2 mutations are also the most common genetic factor in prostate cancers. Since these mutated genes are inherited from parents, identifying the presence of these mutations in healthy family members can help in early diagnosis of breast, ovarian or prostate cancers or prevention of these cancers altogether.</p>            
                                    </div>
                                    <!-- #tab4 --> 

                                    <h4 class="tab_drawer_heading" rel="tab5">Cancer hotspot panel</h4>
                                    <div id="tab5" class="tab_content">
                                        <h3>Cancer hotspot panel</h3>
                                        <p class="theme-description-nlsp">Cancer is a genetic disease.  Genetic testing has now become essential for many different cancers, with the test results being used for diagnosis of certain cancers, as well as for personalized treatment. The Cancer Hotspot Panel includes 50 genes important in several different cancers analyzed in a single test. It is specially useful for Thyroid cancer, Lung cancer, Colon cancer, gastro-intestinal stromal tumors (GIST), Glioma & certain leukemias.</p>            
                                        <br>
                                        <h4>Genes included in the Cancer Hotspot Panel</h4>
                                        <img alt="Cancer Hotspot Panel" class="img-fluid" src="assets/images/cancer-hotspot-panel.jpg">
                                    </div>
                                    <!-- #tab5 --> 

                                    <h4 class="tab_drawer_heading" rel="tab6">Lung Cancer mutation panel</h4>
                                    <div id="tab6" class="tab_content">
                                        <h3>Lung Cancer mutation panel</h3>
                                        <p class="theme-description-nlsp">This test includes analysis of mutations of EGFR Exons 18-21 including EGFR T790M mutation, which is recommended in molecular testing guidelines for targeted treatment of lung cancer patients. Additionally the test also includes BRAF, KRAS, MET & RET mutations that are suggested to be performed if included as part of a larger panel if performed initially, or when routine EGFR, ALK, and ROS1 testing are negative*.</p>            
                                        <p class="theme-description-nlsp special-note">(*Lindeman NI, Cagle PT, Aisner DL et al. Updated molecular testing guideline for the selection of lung cancer patients for treatment with targeted tyrosine kinase inhibitors: guideline from the College of American Pathologists, the International Association for the Study of Lung Cancer, and the Association for Molecular Pathology. Arch Pathol Lab Med. 2018;142(3):321-346)</p>
                                    </div>
                                    <!-- #tab6 --> 

                                    <h4 class="tab_drawer_heading" rel="tab7">RAS Extended Plus for Colorectal Cancer</h4>
                                    <div id="tab7" class="tab_content">
                                        <h3>RAS Extended Plus for Colorectal Cancer</h3>
                                        <p class="theme-description-nlsp">This test includes the analysis of all KRAS & NRAS mutations that are recommended as ‘RAS extended’ testing in international guidelines for targeted treatment of Colorectal cancers*. In addition the test also includes BRAF V600E mutation testing which is recommended for colorectal cancer prognostification, and evaluation of Lynch syndrome risk. Other genes tested in the panel are PIK3CA and PTEN for informational purposes.</p>      
                                        <p class="theme-description-nlsp special-note">(*Molecular Biomarkers for the Evaluation of Colorectal Cancer Guideline From the American Society for Clinical Pathology, College of American Pathologists, Association for Molecular Pathology, and American Society of Clinical Oncology. J Mol Diagn. 2017 Mar; 19(2): 187–225)</p>      
                                        
                                    </div>
                                    <!-- #tab7 --> 

                                    <h4 class="tab_drawer_heading" rel="tab8">Thyroid cancer mutation profile</h4>
                                    <div id="tab8" class="tab_content">
                                        <h3>Thyroid cancer mutation profile</h3>
                                        <p class="theme-description-nlsp">The test includes mutation analysis of BRAF, KRAS, NRAS, HRAS, RET genes that are important in diagnosis of Thyroid cancers</p>            
                                    </div>
                                    <!-- #tab8 --> 

                                    <h4 class="tab_drawer_heading" rel="tab9">AML mutation panel</h4>
                                    <div id="tab9" class="tab_content">
                                        <h3>AML mutation panel</h3>
                                        <p class="theme-description-nlsp">Test for mutations of several genes important in the treatment of acute myeloid leukemia (AML) that consist of NPM1, FLT3-ITD, FLT3-TKD, IDH1, IDH2, KIT & TP53</p>            
                                    </div>
                                    <!-- #tab9 --> 

                                    <h4 class="tab_drawer_heading" rel="tab10">GIST mutation panel</h4>
                                    <div id="tab10" class="tab_content">
                                        <h3>GIST mutation panel</h3>
                                        <p class="theme-description-nlsp">Test for mutations of KIT, PDGFRA & BRAF genes that are important in the treatment of Gastro-intestinal stromal tumors (GIST)</p>            
                                    </div>
                                    <!-- #tab10 --> 

                                    <h4 class="tab_drawer_heading" rel="tab10">Prostate cancer mutation panel</h4>
                                    <div id="tab11" class="tab_content">
                                        <h3>Prostate cancer mutation panel</h3>
                                        <p class="theme-description-nlsp">Test for mutations of BRCA1, BRCA2, ATM, TP53 & MLH1 mutations</p>            
                                    </div>
                                    <!-- #tab11 --> 

                                    <h4 class="tab_drawer_heading" rel="tab10">ABL1 TKI resistance mutations</h4>
                                    <div id="tab12" class="tab_content">
                                        <h3>ABL1 TKI resistance mutations</h3>
                                        <p class="theme-description-nlsp">Test for mutations in the ABL1 gene that are associated with resistance to Tyrosine kinase inhibitor treatment in chronic myeloid leukemia (CML)</p>            
                                    </div>
                                    <!-- #tab12 --> 

                                    <h4 class="tab_drawer_heading" rel="tab10">MPN mutations</h4>
                                    <div id="tab13" class="tab_content">
                                        <h3>MPN mutations</h3>
                                        <p class="theme-description-nlsp">Detection of JAK2 V617F mutation, JAK2 Exon 12 mutation & MPL mutation</p>            
                                    </div>
                                    <!-- #tab13 --> 

                                    <h4 class="tab_drawer_heading" rel="tab10">TP53 mutation</h4>
                                    <div id="tab14" class="tab_content">
                                        <h3>TP53 mutation</h3>
                                        <p class="theme-description-nlsp">Detection of TP53 gene variants that indicate high risk of disease progression and adverse outcomes in patients with chronic lymphocytic leukemia </p>            
                                    </div>
                                    <!-- #tab14 --> 

                                    <h4 class="tab_drawer_heading" rel="tab10">GIST mutation panel</h4>
                                    <div id="tab15" class="tab_content">
                                        <h3>GIST mutation panel</h3>
                                        <p class="theme-description-nlsp">Test for mutations of KIT, PDGFRA & BRAF genes that are important in the treatment of Gastro-intestinal stromal tumors (GIST)</p>            
                                    </div>
                                    <!-- #tab15 --> 

                                    <h4 class="tab_drawer_heading" rel="tab10">IDH1 & IDH2 mutations for glioma</h4>
                                    <div id="tab16" class="tab_content">
                                        <h3>IDH1 & IDH2 mutations for glioma</h3>
                                        <p class="theme-description-nlsp">Test used for the morphological diagnosis of diffuse glioma and prognosis stratification of diffuse glioma as well as acute myeloid leukemia</p>            
                                    </div>
                                    <!-- #tab16 --> 

                                    <h4 class="tab_drawer_heading" rel="tab10">KIT D816V mutation</h4>
                                    <div id="tab17" class="tab_content">
                                        <h3>KIT D816V mutation</h3>
                                        <p class="theme-description-nlsp">Detection of the mutations at D816 position (including D816V) of the KIT gene for diagnosis of systemic mastocytosis    </p>            
                                    </div>
                                    <!-- #tab17 --> 

                                    <h4 class="tab_drawer_heading" rel="tab10">FGFR3 mutation for bladder cancer</h4>
                                    <div id="tab18" class="tab_content">
                                        <h3>FGFR3 mutation for bladder cancer</h3>
                                        <p class="theme-description-nlsp">Tests for FGFR3 mutations for the identification of urothelial tumors that may respond to FGFR-targeted therapies</p>            
                                    </div>
                                    <!-- #tab18 --> 

                                    <h4 class="tab_drawer_heading" rel="tab10">BCR-ABL detection</h4>
                                    <div id="tab19" class="tab_content">
                                        <h3>BCR-ABL detection</h3>
                                        <p class="theme-description-nlsp">A test carried out for patients with suspected BCR-ABL1-positive hematopoietic neoplasms, predominantly chronic myelogenous leukemia (CML) and acute lymphoblastic leukemia</p>            
                                    </div>
                                    <!-- #tab19 --> 

                                    <h4 class="tab_drawer_heading" rel="tab10">PML-RARA detection</h4>
                                    <div id="tab20" class="tab_content">
                                        <h3>PML-RARA detection</h3>
                                        <p class="theme-description-nlsp">Detection of PML-RARA translocation for the diagnosis of acute promyelocytic leukemia (APL)</p>            
                                    </div>
                                    <!-- #tab20 --> 

                                    <h4 class="tab_drawer_heading" rel="tab10">ALL translocation panel</h4>
                                    <div id="tab21" class="tab_content">
                                        <h3>ALL translocation panel</h3>
                                        <p class="theme-description-nlsp">Detection of BCR-ABL, t(1;19), t(12;21) & t(4;11) translocations for treatment and prognostic stratification of Acute Lymphocytic Leukemia (ALL)</p>            
                                    </div>
                                    <!-- #tab21 --> 

                                    <h4 class="tab_drawer_heading" rel="tab10">AML translocation panel</h4>
                                    <div id="tab22" class="tab_content">
                                        <h3>AML translocation panel</h3>
                                        <p class="theme-description-nlsp">Detection of t(15;17), Inv(16) & t(8;21) translocations for treatment and prognostic stratification of Acute Myeloid  Leukemia (AML)</p>            
                                    </div>
                                    <!-- #tab22 --> 

                                    <h4 class="tab_drawer_heading" rel="tab10">HLA typing</h4>
                                    <div id="tab23" class="tab_content">
                                        <h3>HLA typing</h3>
                                        <p class="theme-description-nlsp">Typing of HLA-A, B & DR for donor and recipient matching for organ transplantation purposes</p>            
                                    </div>
                                    <!-- #tab23 --> 

                                    <h4 class="tab_drawer_heading" rel="tab10">Paternity testing</h4>
                                    <div id="tab24" class="tab_content">
                                    <h3>Paternity testing</h3>
                                    <p class="theme-description-nlsp">DNA Paternity test at Genelabs Medical provide you with accurate and reliable paternity results while ensuring your confidentiality. Our paternity test analyzes 13 CODIS (Combined DNA Index System) loci and 3 other additional loci, to give you a peace of mind with accurate paternity answers. </p>
                                    <p class="theme-description-nlsp">We also offer an at-home sample collection package for those who wish to keep the identity more confidential.</p>
                                    <h4 class="mt-4">How to get a paternity test</h4>

                                         <div>
                                            <ul class="test-step pl-3">
                                                <li>
                                                    <p class="step-tag">Step 01</p>
                                                    <p class="theme-description-nlsp">Call Genelabs Medical. We will explain the entire process of the paternity test. You have the choice of ordering an at-home paternity test sample collection package, or visit Genelabs Medical to provide us with the required swab samples</p>
                                                </li>
                                                <li>
                                                    <p class="step-tag">Step 02</p>
                                                    <p class="theme-description-nlsp">If choosing at-home collection option; we will ship the sample collection kit to you along with collection instructions, where you need to ship it back to us.</p>
                                                    <p class="theme-description-nlsp">If visiting Genelabs Medical; we will collect the samples and process them.</p>
                                                </li>
                                                <li>
                                                    <p class="step-tag">Step 03</p>
                                                    <p class="theme-description-nlsp">We will issue the results within 3 weeks. You can obtain the results via email, post or simply visit Genelabs medical at your convenience.</p>
                                                </li>
                                            </ul>

                                            <p class="theme-description-nlsp">Disclaimer: Our paternity test reports are for personal information only, and not for legal purposes.</p>
                                        </div>           
                                    </div>
                                    <!-- #tab24 --> 

                                    


                                    </div>
                                <!-- .tab_container -->
                                </div>
                                

                            </div>
                            
                        </div>
                    </div>  
                </div>
            </div>
        </div>

    </main>

    <?php include 'includes/page-footer.php'; ?>
    <?php include 'includes/common-scripts.php'; ?>

    <script>
        // tabbed content
    $(".tab_content").hide();
    $(".tab_content:first").show();

  /* if in tab mode */
    $("ul.tabs li").click(function() {
		
      $(".tab_content").hide();
      var activeTab = $(this).attr("rel"); 
      $("#"+activeTab).fadeIn();		
		
      $("ul.tabs li").removeClass("active");
      $(this).addClass("active");

	  $(".tab_drawer_heading").removeClass("d_active");
	  $(".tab_drawer_heading[rel^='"+activeTab+"']").addClass("d_active");
	  
    });
	/* if in drawer mode */
	$(".tab_drawer_heading").click(function() {
      
      $(".tab_content").hide();
      var d_activeTab = $(this).attr("rel"); 
      $("#"+d_activeTab).fadeIn();
	  
	  $(".tab_drawer_heading").removeClass("d_active");
      $(this).addClass("d_active");
	  
	  $("ul.tabs li").removeClass("active");
	  $("ul.tabs li[rel^='"+d_activeTab+"']").addClass("active");
    });
	
	
	/* Extra class "tab_last" 
	   to add border to right side
	   of last tab */
    $('ul.tabs li').last().addClass("tab_last");
    
    
        </script>
</body>
</html>
