(function ($) {
    "use strict";

    // open - close more menu
    // $('#header .menu-icon').on('click', function() {
    //     if ( !$('.more-menu-wr').hasClass('open') ) {
    //         $('.more-menu-wr').addClass('open');
    //         $('.open-more-overlay').addClass('show');
    //     }
    // });
    // $('.open-more-overlay').on('click', function() {
    //     if ( $('.more-menu-wr').hasClass('open') ) {
    //         $('.more-menu-wr').removeClass('open');
    //         $('.open-more-overlay').removeClass('show');
    //     }
    // });

    //Show / Hide search bar
    var TriggerClick = 0;
    $('.gene-search-btn').on('click', function() {
        $('.gene-search-bar').stop();
        if(TriggerClick==0){
            TriggerClick=1; 
            $('.gene-search-bar').animate({ 
                width: '150px'
            });
        }else{
            TriggerClick=0;
            if($('.gene-search-bar').val() <= 0){
                $('.gene-search-bar').animate({ 
                    width: '0'
                }); 
            } else{
                $('.gene-search-bar').closest('form').submit();
            }
        };
        return false;
    });

     
    // $("#td-header-search-button").click(function(){
        
    // });


    // Tab scroll on mobiles - Our Services
    $(document).on('click', '.tab-name', function(event) {
        if ($(window).width() < 767) {
            $('html, body').animate({
            scrollTop: $('.tab-scroll-to').offset().top - 0
            }, 1000);
            return false;
        }                
    });

    $(document).on('click', '.tab-name-bt', function(event) {
        if ($(window).width() > 768) {
            $('html, body').animate({
            scrollTop: $('.tab-scroll-to').offset().top - 0
            }, 1000);
            return false;
        }                
    });


    $(document).on('click', '.move-down', function(event) {
        $('html, body').animate({
            scrollTop: $('.inner-page-content').offset().top - 0
        }, 1000);
        return false;
    });

}(jQuery)); 

// Select navigation
$(document).ready(function(){
    
    var path = window.location.pathname.split("/").pop();
    if (path == '') {
        path = 'index.php';
    }
    var target = $('nav a[href="'+path+'"]')
    target.addClass('active');


 
    $(window).scroll(function () {
        if ($(this).scrollTop() > 400) {
            $('#back-to-top').fadeIn();
        } else {
            $('#back-to-top').fadeOut();
        }
    });
    // scroll body to 0px on click
    $('#back-to-top').click(function () {
        $('body,html').animate({
            scrollTop: 0
        }, 400);
        return false;
    });


  });

 
  
// Home Page slider
$('.main-slider').owlCarousel({
    loop:true,
    margin:0,
    items:1,
    nav:false,
    responsive:{
        0:{
            // items:1
        },
        600:{
            // items:3
        },
        1000:{
            // items:5
        }
    }
});

// Client Slider(Home page)
$('.client-carousel').owlCarousel({
    loop:true,
    dots: true,
    margin:10,
    nav:false,
    items:1,
    responsive:{
        0:{
            items:3,
            nav:false,
            dots: false,
            nav:true
        },
        600:{
            items:3
        },
        1000:{
            items:3
        }
    }
});